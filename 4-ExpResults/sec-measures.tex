\section{Measures and considerations}
\label{sec:measures}

\subsection{Native response time}
\label{subs:native-time}

The main goal of this work was to integrate the \emph{native code} into the
\emph{Java} environment, which is the default one for Android applications.\\
Some tests were performed, with respect to some native commands, as:
\begin{itemize}
	\item \texttt{EXCisRegistered}
	\item \texttt{EXCCreate}
	\item \texttt{EXCStart}
\end{itemize}

Two main situations can be considered, to analyse some performances and, in
particular, the overhead added to the native code: we sampled the execution time
for the \emph{EXC} methods (native calls through JNI), and the execution time
for the Java methods within the service that, in turn, call the \emph{EXC}
methods.\\
In Table \ref{tab:exc-registered} we can see the added time needed to perform
the native call through the JNI mechanism from the Java environment, which was
calculated in $227.033\pm$23\us.

\begin{table}[h]
		\centering
		\begin{tabular}{c|c|c|c|c}
				\hline
		\# samples&min time [\us]&max time [\us]&avg time[\us]&CI 99\%
				[\us]\\
				\hline
				30&186&417&227.033&$\pm23.004$\\
				\hline
		\end{tabular}
		\caption{\texttt{EXCisRegistered} timings on the sample application
		BbqueActivity on 30 samples}
		\label{tab:exc-registered}
\end{table}

This native call is performed within a Java call, which runs within the
BbqueService: it's interesting to analyse how much time overhead is added by the
whole Java procedure, from the actual Java call, to the response, and an example
can be seen in Table \ref{tab:registered}, where are shown the statistic data
regarding 30 samples. In addition to data, the correspondent graphical
representation of the table is shown in Figure \ref{fig:isRegistered-tv}, where
the \emph{traceview} output can be found.

\begin{table}[h]
		\centering
		\begin{tabular}{r|c|c|c}
			\hline
			call&avg time[\us]&$\sigma$&CI 99\% [\us]\\
			\hline
			\texttt{isRegistered}&1051.233&244.352&$\pm 115.100$\\
			\hline
				Messenger.send&347.700&144.251&$\pm 67.948$\\
				EXCisRegistered&227.033&48.836&$\pm 23.004$\\
				Log.d&225.567&29.376&$\pm 13.837$\\
				Message.obtain&69.900&10.512&$\pm 4.952$\\
			\hline
		\end{tabular}
		\caption{\texttt{BbqueService.isRegistered()} timings on the sample
		application BbqueActivity, with detail of inner children methods on 30
		samples}
		\label{tab:registered}
\end{table}

\begin{figure}[h]
	\centering
	\includegraphics[width=\widefigure]{images/traceview-isRegistered.eps}
	\caption[\texttt{isRegistered()} execution stack - traceview]
			{\figurecaption{\texttt{isRegistered()} execution stack - traceview}}
	\label{fig:isRegistered-tv}
\end{figure}

The same considerations will be done for the \texttt{EXCCreate} function, which
is the one that creates the execution context. Data on 60 samples are given
in Table \ref{tab:exc-create}. The native call executes in an average time
of $1.463\pm$ 0.187ms.

\begin{table}[h]
		\centering
		\begin{tabular}{c|c|c|c|c}
				\hline
		\# samples&min time [\us]&max time [\us]&avg time[\us]&CI 99\%
				[\us]\\
				\hline
				60&1042&3807&1463.317&$\pm 187.423$\\
				\hline
		\end{tabular}
		\caption{\texttt{EXCCreate} timings on the sample application
		BbqueActivity} on 60 samples.
		\label{tab:exc-create}
\end{table}

The aforementioned results can be contextualised within the "parent" Java call,
which is typically executed by the \texttt{BbqueService.create()} method. The
results shown in Table \ref{tab:create} refer to the native result
\emph{BbqueActivity} of Table \ref{tab:exc-create}.

\begin{table}[h]
		\centering
		\begin{tabular}{r|c|c|c}
			\hline
			call&avg time[\us]&$\sigma$&CI 99\% [\us]\\
			\hline
			\texttt{create}&4925.017&672.085&$\pm 223.856$\\
			\hline
				EXCCreate&1463.317&562.702&$\pm 187.423$\\
				String.split&1383.267&632.963&$\pm 210.825$\\
				StringBuilder.append&243.061&72.662&$\pm 13.973$\\
				Messenger.send&417.550&243.547&$\pm 81.120$\\
				Log.d&229.350&28.768&$\pm 9.582$\\
				Message.obtain&106.517&196.555&$\pm 65.468$\\
			\hline
		\end{tabular}
		\caption{\texttt{BbqueService.create()} timings on the sample
		application \textit{BbqueActivity} on 60 samples}
		\label{tab:create}

\end{table}

\begin{figure}[h]
	\centering
	\includegraphics[width=\widefigure]{images/traceview-create.eps}
	\caption[\texttt{create()} execution stack - traceview]
			{\figurecaption{\texttt{create()} execution stack - traceview}}
	\label{fig:create-tv}
\end{figure}

In addition to the basic information we can get from data, we can notice that a
simple split operation on a string costs a lot, if compared to the main
operation run by this method, almost the same amount of time.\\
Generally the \texttt{EXCCreate} takes much more time than the
\texttt{isRegistered} above, because of its JNI implementation, where the native
function operates on strings, to retrieve the application name and the recipe.

The last native call that was mentioned at the beginning of this chapter, is the
\texttt{EXCStart}. As done for the previous functions its time profiling is
shown at Table \ref{tab:exc-start}. The traceview process execution stack of the
Java \texttt{start} method (which, in turn, calls the \texttt{EXCStart}) is
shown at Figure \ref{fig:start-tv}, and statistics out of 30 \emph{start}
samples is given at Table \ref{tab:start}.

\begin{table}[h]
		\centering
		\begin{tabular}{c|c|c|c|c}
				\hline
				\# samples&min time [\us]&max time [\us]&avg
				 time[\us]&CI 99\% [\us]\\
				\hline
				30&554&1737&793.200&$\pm 126.380$\\
				\hline
		\end{tabular}
		\caption{\texttt{EXCStart} timings on the sample application
		BbqueActivity} on 30 samples.
		\label{tab:exc-start}
\end{table}

\begin{table}[h]
		\centering
		\begin{tabular}{r|c|c|c}
			\hline
			call&avg time[\us]&$\sigma$&CI 99\% [\us]\\
			\hline
			\texttt{start}&1615.233&350.317&$\pm 165.014$\\
			\hline
				EXCStart&793.200&268.299&$\pm 126.380$\\
				Messenger.send&379.700&157.081&$\pm 73.992$\\
				Log.d&234.917&55.385&$\pm 18.447$\\
				Message.obtain&72,933&15.929&$\pm 7.503$\\
			\hline
		\end{tabular}
		\caption{\texttt{BbqueService.start()} timings on the sample application
\textit{BbqueActivity} on 30 samples}
		\label{tab:start}
\end{table}
\begin{figure}[h]
	\centering
	\includegraphics[width=\widefigure]{images/traceview-start.eps}
	\caption[\texttt{start()} execution stack - traceview]
			{\figurecaption{\texttt{start()} execution stack - traceview}}
	\label{fig:start-tv}
\end{figure}

Some considerations about the aforementioned methods: being those calls embedded
within the \emph{BbqueService}, they generally won't be overridden, therefore
their performances can vary only depending on the running architecture, not on
the implementation. They will also likely be called not frequently, therefore
the small overhead added by the Java/JNI code can be considered absolutely
reasonable.

%It's worth considering a callback method, to measure the responsiveness of this
%implementation. The testing application was modified with an \emph{empty}
%\texttt{onRun} method, to leave it as much lightweight as possible, and time
%intervals between 

\subsection{Messaging system overhead and performances}
\label{subs:messaging}

As described in \ref{ssub:messaging} the Messenger class was used to pass
messages among processes, in particular between the activity and the service,
and vice versa.\\
It's interesting to analyse how this mechanism can influence performances and
completion time. Data were sampled, again, from the \emph{BbqueActivity}
performing basic operations, like the \texttt{isRegistered}, the \texttt{create}
and the \texttt{start} methods.\\
The first thing that pops up, among data, is that the delivery time of messages
can vary between one or even two orders of magnitude: this is because messages
are continuously used by \emph{Android} as an IPC method, and they don't have
priorities nor any parallelism. By the tests here conducted, for example, some
of the messages were enqueued behind system ones, therefore they were affected
by the execution time of system functionalities, as UI interactions. This
appears to be a huge limitation of this messaging system, but still we have to
consider the delay it introduces within the context of processing big streams of
data which, once the computation is started, is an atomic activity.\\
In the tables that follow, calls stack with in/out methods timings is shown.\\
As an example, if we consider the \texttt{isRegistered} call, which starts from
a UI button, and we measure the time which elapses between the message sending,
to the correspondent actual method execution within the service class, we see
that a message, to be delivered and its action triggered, takes a time which is
one order of magnitude bigger than the time needed for the execution of the
method itself. As will be discussed later, however, this impact is almost
negligible, especially when compared to the main computation that typically is
performed within core methods of the service, as the \emph{onRun}.

\begin{table}[h]
	\centering
	\begin{tabular}{p{180px}|c|c|c|c}
			\texttt{btnIsRegistered}&IN [ms]&OUT [ms]&$\delta$(i-1)&$\delta$\\
		\hline
		\textit{Messenger.send}&1434.054&1434.376&-&0.322\\
		\hline
		\multicolumn{5}{c}{\textit{Other operations executed for UI
		rendering, through message sending}}\\
		\hline
		\textit{Message.dispatch}&1446.117&1447.881&11.741&1.764\\
		\textit{CustomService.CustomMessageHandler}&1446.135&1447.863&0.018&
			1.728\\
		\textit{BbqueService.BbqueMessageHandler}&1446.155&1447.847&0.020&
			1.692\\
		\textit{BbqueService.isRegistered}&1446.175&1447.829&0.020&1.654\\
		\hline
		\multicolumn{3}{l}{\textit{Time from "Messenger.send" end to
		"isRegistered" start}}&\textbf{11.799}&\\
		\hline
	\end{tabular}
	\caption{Button \textit{isRegistered?} press: timings on the sample
			application. IN = instant method enters, OUT = instant method
			exits, $\delta$(i-1) = IN(i)-IN(i-1), $\delta$ = OUT(i)-IN(i)}
	\label{tab:msg-registered}
\end{table}

Before considering more data samples, it's worth noticing that the \emph{message
handler} implementation within the \texttt{BbqueService} described in
\ref{ssub:messaging}, when extended by the \texttt{CustomService} it brings an
additional cost in terms of computation time, since the \texttt{switch}
structure will be repeated twice: this comes clear at Table
\ref{tab:msg-registered}, where there's a double 18-20 microseconds time.\\
In addition to the single example given so far, we provide a statistic which
is shown in Table \ref{tab:msg-registered2}, where times corresponding to the
last line of Table \ref{tab:msg-registered} were sampled on 30 runs.

\begin{table}[h]
		\centering
		\begin{tabular}{c|c|c|c|c|c}
				\hline
				\# samples&min [ms]&max [ms]&avg [ms]&$\sigma$&CI 99\% [ms]\\
				\hline
				30&11.054&17.104&12.552&1.334&$\pm 0.618$\\
				\hline
		\end{tabular}
		\caption{Time elapsed from "Messenger.send" exit to "isRegistered"
		enter, on the sample application BbqueActivity on 30 samples.}
		\label{tab:msg-registered2}
\end{table}

Given these data, we can consider the Messenger protocol very fast and reliable,
with the only limitation of being sequential and queue-based without any
possibility to set a priority, being used by the operative system as well.

The same considerations can be done for the remaining methods we took into
consideration previously, the \emph{create} and the \emph{start}. Details on
time needed to a message to trigger the call are shown in Tables
\ref{tab:msg-create1} \emph{(create)}, \ref{tab:msg-start1} \emph{(start)} and
the statistics on message delivery timings are shown in Tables
\ref{tab:msg-create2} \emph{(create)}, \ref{tab:msg-start2} \emph{(start)}.
%CREATE
\begin{table}[h]
	\centering
	\begin{tabular}{p{180px}|c|c|c|c}
			\texttt{btnCreate}&IN [ms]&OUT [ms]&$\delta$(i-1)&$\delta$\\
		\hline
		\textit{Messenger.send}&1380.490&1380.787&-&0.297\\
		\hline
		\multicolumn{5}{c}{\textit{Other operations executed for UI
		rendering, through message sending}}\\
		\hline
		\textit{Message.dispatch}&1393.090&1397.626&12.303&4.536\\
		\textit{CustomService.CustomMessageHandler}&1393.108&1397.607&0.018&
			4.499\\
		\textit{BbqueService.BbqueMessageHandler}&1393.126&1397.588&0.018&
			4.443\\
		\textit{BbqueService.create}&1393.145&1397.569&0.019&4.424\\
		\hline
		\multicolumn{3}{l}{\textit{Time from "Messenger.send" end to
		"create" start}}&\textbf{12.358}&\\
		\hline
	\end{tabular}
	\caption{Button \textit{Create} press: timings on the sample
			application. IN = instant method enters, OUT = instant method
			exits, $\delta$(i-1) = IN(i)-IN(i-1), $\delta$ = OUT(i)-IN(i)}
	\label{tab:msg-create1}
\end{table}

\begin{table}[h]
		\centering
		\begin{tabular}{c|c|c|c|c|c}
				\hline
				\# samples&min [ms]&max [ms]&avg [ms]&$\sigma$&CI 99\% [ms]\\
				\hline
				60&11.591&42.381&13.501&4.006&$\pm 1.334$\\
				\hline
		\end{tabular}
		\caption{Time elapsed from "Messenger.send" exit to "create"
		enter, on the sample application BbqueActivity on 60 samples.}
		\label{tab:msg-create2}
\end{table}

%START
\begin{table}[h]
	\centering
	\begin{tabular}{p{180px}|c|c|c|c}
			\texttt{btnStart}&IN [ms]&OUT [ms]&$\delta$(i-1)&$\delta$\\
		\hline
		\textit{Messenger.send}&1822.847&1827.347&-&4.500\\
		\hline
		\multicolumn{5}{c}{\textit{Other operations executed for UI
		rendering, through message sending}}\\
		\hline
		\textit{Message.dispatch}&1840.771&1842.979&13.424&2.208\\
		\textit{CustomService.CustomMessageHandler}&1840.794&1842.961&0.023&
			2.167\\
		\textit{BbqueService.BbqueMessageHandler}&1840.814&1842.943&0.020&
			2.129\\
		\textit{BbqueService.start}&1840.833&1842.925&0.019&2.092\\
		\hline
		\multicolumn{3}{l}{\textit{Time from "Messenger.send" end to
		"start" start}}&\textbf{13.486}&\\
		\hline
	\end{tabular}
	\caption{Button \textit{Start} press: timings on the sample
			application. IN = instant method enters, OUT = instant method
			exits, $\delta$(i-1) = IN(i)-IN(i-1), $\delta$ = OUT(i)-IN(i)}
	\label{tab:msg-start1}
\end{table}

\begin{table}[h]
		\centering
		\begin{tabular}{c|c|c|c|c|c}
				\hline
				\# samples&min [ms]&max [ms]&avg [ms]&$\sigma$&CI 99\% [ms]\\
				\hline
				30&12.586&53.410&15.861&7.261&$\pm 3.420$\\
				\hline
		\end{tabular}
		\caption{Time elapsed from "Messenger.send" exit to "start"
		enter, on the sample application BbqueActivity on 30 samples.}
		\label{tab:msg-start2}
\end{table}

The data above confirm the generally small time overhead of the integration
implementing choice: we will see later that the time needed to the
activity-service paradigm to communicate is acceptable in the overall
performance context.\\
Before continuing, at Table \ref{tab:msg-onrun} the statistic data about the
onRun callback maximum rate are given: the elapsed time between two \emph{onRun}
callbacks was measured on 100 samples. Notice that testing performances are
likely highly affected by the Android tracing system (which sometimes
dramatically slows down performances) and by the emulated environment. All tests
should be run on the real target platform, to have a more reliable statistic.

\begin{table}[h]
		\centering
		\begin{tabular}{c|c|c|c|c|c}
				\hline
				\# samples&min [ms]&max [ms]&avg [ms]&$\sigma$&CI 99\% [ms]\\
				\hline
				100&8.432&12.268&9.755&0.789&$\pm 0.204$\\
				\hline
		\end{tabular}
		\caption{Time elapsed between two onRun executions on the sample
		application BbqueActivity on 100 samples.}
		\label{tab:msg-onrun}
\end{table}

With these data, we could consider as a limit case where the \emph{onRun} method
computing tends to zero, the processing rate as high as 100 operations per
second. This is obviously a theoretical value (based on an emulation).

\subsection{STHormFaceDetection Application: performance profiling}
\label{subs:traceview}

As mentioned in Section \ref{sec:sthorm-fd}, the application that was used to
test the integration of \emph{Barbeque} is a \emph{Face Detection} application,
developed by \emph{STMicroelectronics}.\\
We had the opportunity to run the Java application on the system simulator
provided by STMicroelectronics \cite{Benini2012}, and some performance data were
collected, and are shown in the following tables. The data we present here
concern the mere method execution, but the actual time perceived by the user is
bigger, because of other operation that are executed in the meanwhile.\\
We let the application run and process 30 frames, with and without the face
detection filter, and results can be seen at Tables \ref{tab:msg-onrun-filter}
and \ref{tab:msg-onrun-nofilter}. In Figure \ref{fig:onRun-filter} a graphical
representation of the 30 samples is shown, with focus on the main processing
methods and their execution elapsed time with respect to each \emph{onRun}.

\begin{table}[h]
		\centering
		\begin{tabular}{r|c|c|c|c|c}
				\hline
				Function&min [ms]&max [ms]&avg [ms]&$\sigma$&CI 99\% [ms]\\
				\hline
				\texttt{onRun}&166.254&209.550&189.052&14.706&$\pm 6.927$\\
				\texttt{decodeYUV}&132.859&158.004&142.547&5.650&$\pm 2.662$\\
				\texttt{FDetectRun}&1.271&4.766&1.6282&0.630&$\pm 0.297$\\
				\hline
		\end{tabular}
		\caption{BbqueFDApplication - 30 frames processing with FD filter ON}
		\label{tab:msg-onrun-filter}
\end{table}

\begin{figure}[h]
	\centering
	\includegraphics[width=\widefigure]{images/onRun-graph.eps}
	\caption[Performance of the DecodeYUV420SP/ FDetectRun wrt \texttt{onRun}]
			{\figurecaption{Performance of the DecodeYUV420SP and FDetectRun
			with respect to the \texttt{onRun}}}
	\label{fig:onRun-filter}
\end{figure}

\begin{table}[h]
		\centering
		\begin{tabular}{r|c|c|c|c|c}
				\hline
				Function&min [ms]&max [ms]&avg [ms]&$\sigma$&CI 99\% [ms]\\
				\hline
				\texttt{onRun}&158.898&200.605&176.262&10.959&$\pm 5.162$\\
				\texttt{decodeYUV}&139.322&167.840&151.780&6.029&$\pm 2.840$\\
				\hline
		\end{tabular}
		\caption{BbqueFDApplication - 30 frames processing with FD filter OFF}
		\label{tab:msg-onrun-nofilter}
\end{table}

As mentioned before, perceived execution time appears being much higher, and it
empirically is around 3 to 4 seconds: this aspect is still under investigation.

In conclusion, it's worth considering that all the \emph{tracing} mechanism slew
down the whole execution, therefore the data provided so far should be
considered as a worst case and, in general, correct with regard to the
proportions and not necessarily to the absolute values. The \texttt{onRun}, for
instance, from the logcat, goes almost twice faster. More precise measures can
be collected when the application will be running on the real platform.
