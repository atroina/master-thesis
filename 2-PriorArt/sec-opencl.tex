\section{OpenCL}
\label{sec:opencl}

Speaking of heterogeneous systems, we discussed how these platforms can
perform tremendously good, if properly exploited.\\
A natural question is why these many-core processors are so fast compared to
traditional single core CPUs.
\begin{itemize}
	\item The fundamental driving force is innovative parallel hardware.\\
			Many-core processors organize their - billions of - transistors into
			many parallel processors, consisting of hundreds of floating point
			units
	\item Another important reason for their speed advantage is new parallel
			software. 
\end{itemize}
Computing systems come out of the - hopefully perfect - combination of hardware
and software: without a proper software which can exploit at best all the
available resources, state-of-the-art hardware can be useless. Parallel hardware
delivers performance by running multiple operations at the same time. To be
useful, parallel hardware needs software that executes as multiple streams of
operations running at the same time; in other words, you need parallel
software.\\
C language nicely abstracts a sequential computer, but to fully exploit
heterogeneous computers, new programming models came out, which can abstract a
modern parallel computer: typically techniques established so far in graphics
can be used as a guide towards this path.\\
And here we come with \emph{OpenCL}: few lines of story behind this programming
language. Going back to some years ago, a project named \emph{Brook for GPU}
took life at Stanford University: the basic idea behind \emph{Brook} was to
treat GPU as a data-parallel processor. Brook was built as a proof of concept,
nevertheless Ian Buck, a graduate student at Stanford, went on to \emph{NVIDIA}
to develop \emph{CUDA}, an extension of Brook, which introduced many
improvements to its predecessor as, for instance, the concept of cooperating
thread arrays, or thread blocks.

\emph{OpenCL (Open Computing Language)} - born in 2008 - provides a logical
extension of the core ideas from GPU Computing, the era of ubiquitous
heterogeneous parallel computing.  OpenCL has been carefully designed by the
Khronos Group with input from many vendors and software experts. OpenCL benefits
from the experience gained using CUDA in creating a software standard that can
be implemented by many vendors.  OpenCL implementations run now on widely used
hardware, including CPUs and GPUs from NVIDIA, AMD, and Intel, as well as
platforms based on DSPs and FPGAs.\\
With OpenCL, you can write a single program that can run on a wide range of
systems, from cell phones, to laptops, to nodes in massive super-computers. No
other parallel programming standard has such a wide reach. This is one of the
reasons why OpenCL is so important.

OpenCL was defined with two different programming models in mind:
task parallelism and data parallelism, thus we can even think
in terms of a hybrid model: tasks that contain data parallelism.

\begin{itemize}
	\item In a data-parallel programming model, programmers think of their
			problems in terms of collections of data elements that can be
			updated concurrently. The parallelism is expressed by concurrently
			applying the same stream of instructions (a task) to each data
			element. The parallelism is in the data.
	\item In a task-parallel programming model, programmers directly define and
			manipulate concurrent tasks. Problems are decomposed into tasks that
			can run concurrently, which are then mapped onto processing elements
			(PEs) of a parallel computer for execution. This is easiest when the
			tasks are completely independent, but this programming model is also
			used with tasks that share data. The computation with a set of tasks
			is completed when the last task is done.
\end{itemize}

\subsection{Platform model}
\label{subs:ocl-platform-model}
The OpenCL platform model defines a high-level representation of any
heterogeneous platform used with OpenCL, and it's shown in Figure
\ref{fig:ocl-platform}

\begin{figure}[h]
	\centering
	\includegraphics[width=\mediumfigure]{images/ocl-platform.eps}
	\caption{Platform model}
	\label{fig:ocl-platform}
\end{figure}

An OpenCL platform always includes a single \emph{Host} which interacts with the
environment external to the OpenCL program, and is connected to one or more
\emph{OpenCL devices}. The device - can be a CPU, a GPU, a DSP...  - is where
the streams of instructions (called \emph{kernels}) are actually executed. The
OpenCL devices are further divided into \emph{compute units} which are
further divided into one or more \emph{processing elements (PEs)}, where the
actual computations really are performed.

\subsection{Execution model}
\label{subs:ocl-execution-model}

An OpenCL application consists of two distinct parts: the \emph{host program}
and a collection of one or more \emph{kernels}. The OpenCL execution model
defines how an OpenCL application maps onto processing elements, memory regions,
and the host.  The \emph{host program} runs on the host, the kernels execute on
the OpenCL devices. They do the real work of an OpenCL application. Kernels are
typically simple functions that transform input memory objects into output
memory objects. OpenCL defines two types of kernels:
\begin{itemize}
	\item OpenCL kernels: functions written with the OpenCL C programming
			language and compiled with the OpenCL compiler. All OpenCL
			implementations must support OpenCL kernels.
	\item Native kernels: functions created outside of OpenCL and accessed
			within OpenCL through a function pointer. These functions could be,
			for example, functions defined in the host source code or exported
			from a specialized library.
\end{itemize}

\subsection{Context}
\label{subs:ocl-context}

The computational work of an OpenCL application takes place on the
OpenCL devices. The host, however, plays a very important role in the
OpenCL application. It is on the host where the kernels are defined. The
host establishes the context for the kernels.
As the name implies, the context defines the environment within
which the kernels are defined and execute. To be more precise, we define
the context in terms of the following resources:
\begin{itemize}
	\item Devices: the collection of OpenCL devices to be used by the host
	\item Kernels: the OpenCL functions that run on OpenCL devices
	\item Program objects: the program source code and executables that
			implement the kernels. Program objects are built at runtime.
	\item Memory objects: a set of objects in memory that are visible to OpenCL
			devices and contain values that can be operated on by instances of a
			kernel.  These are explicitly defined on the host and explicitly
			moved between the host and the OpenCL devices.
\end{itemize}
