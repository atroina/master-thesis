\section{Java Native Interface (JNI)}
\label{sec:jni}

As mentioned in Chapter \ref{cha:Overview} the element which acts as a bridge
between the Java and the Native world, is JNI, initially released in early 1997.
With JNI, the developer can achieve two main goals: reusing her native code
within a Java environment, and optimizing the execution with regard to
performances, so that intensive operations can run natively, instead of being
interpreted, as Java pattern requires - except for the peculiar case of the
\textit{JIT-ed} code, where the bytecode is compiled \textit{Just In Time} to
run natively (this operation commonly runs at launch time, but can happen at
install time, or at method invoke time).\\
The JNI is a powerful feature that allows you to take advantage of the Java
platform, but still utilize code written in other languages. As a part of the
Java virtual machine implementation, the JNI is a \emph{two-way} interface that
allows Java applications to invoke native code and vice versa.\\
The JNI is designed to handle situations where you need to combine Java
applications with native code, and it can support two types of native code:
native \emph{libraries} and native \emph{applications}.

\begin{itemize}
	\itemsep 0em
	\item JNI can be used to write \emph{native methods} that allow Java
			applications to call functions implemented in native libraries: Java
			applications call native methods in the same way that they call
			methods implemented in the Java programming language. Behind the
			scenes, however, native methods are implemented in another language
			and reside in native libraries.
	\item JNI supports an invocation interface that allows you to embed a Java
			virtual machine implementation into native applications. Native
			applications can link with a native library that implements the Java
			virtual machine, and then use the invocation interface to execute
			software components written in the Java programming language. Part
			of this feature can be seen as the use of the Java methods callbacks
			that will be illustrated in the next chapters.
\end{itemize}

JNI is an interface that can be supported by all Java virtual machine
implementations on a wide variety of host environments. With the JNI:
\begin{itemize}
	\item Each virtual machine implementor can support a larger body of native
			code.
	\item Development tool vendors do not have to deal with different kinds of
			native method interfaces.
	\item Most importantly, application programmers are able to write one
			version of their native code and this version will run on different
			implementations of the Java virtual machine.
\end{itemize}

\subsection{Typical JNI use}
\label{sub:jni}
JNI per se basically needs two components to be used: the \textit{javah} JDK
tool, which builds c-style header files from a given Java class (that will be
implemented afterwards in a proper native source file, which includes the
mentioned header), and the \textit{jni.h} header file, which maps the Java types
to their native counterparts. The whole flow (shown in Figure
\ref{fig:jni-flow}) mainly lies in four steps:
\begin{itemize}
	\itemsep 0em
	\item implement a \textit{Java class}, declare the methods you want to call
			on the native environment as \texttt{native}, and compile it
	\item generate the header file through the \texttt{javah -jni} command
	\item implement as native C/C++ code the function whose signatures have been
			generated during the step above
	\item compile the file above as a shared library, which will be loaded by
			the java class
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=\mediumfigure]{images/jni-flow.eps}
	\caption{JNI flow}
	\label{fig:jni-flow}
\end{figure}

\subsection{JNI under Android: NDK}
\label{sub:ndk}
It is frequent, for developers who work on the Android framework - especially
when their device introduces some new hardware components - the need for
accessing to native functions, or drivers, from the Java side. Hence Android can
be seen as a typical example of a situation where Java and Native worlds have to
strictly cooperate: this is where using JNI can be useful (although each
developer must carefully decide whether to use it, or to choose any possible
alternative instead of embedding portions of native code, such as using already
implemented Java libraries or interfaces). This being said, there's a toolset
that lets developers embed components that make use of native code into Android
applications very easily, with no need to necessarily follow the typical JNI
steps mentioned above: this toolkit is named \emph{"Android Native Development
Kit}" (NDK), which  can be found at:
\url{http://developer.android.com/tools/sdk/ndk/index.html}). The use of the NDK
condenses the steps above in just one main step, which will do almost everything
at once, through the \texttt{ndk-build} command.\\
Under Android it becomes very easy to embed native code and, basically, the
procedure can be resumed as follows:

\begin{itemize}
		\item Download and install the Android NDK (one shot action)
		\item Develop your Android application, declaring the functions you want
				to access to as \texttt{native} methods, and use them as if they
				were (as actually are) Java methods
		\item Write your native code within a \texttt{jni} folder, and write an
				appropriate makefile (\texttt{Android.mk}) to export the
				compiled code as shared library
		\item Launch the \texttt{ndk-build} command within the application
				folder
		\item Launch your application, and that's all.
\end{itemize}

A detailed description and some examples of JNI implementations can be found in
Appendix \ref{app:Tutorials}
