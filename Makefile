
SOURCE = thesis
OBJS = bbl ilg ind blg glo out aux dvi logog ps backup bak toc lof idx log DS_Stor

all: pdf report

preview:
	TEXINPUTS="./styles:" latexmk -dvi -pvc -silent $(SOURCE).tex


dvi: $(SOURCE).dvi $(SOURCE).bbl
$(SOURCE).dvi: $(SOURCE).tex $(SOURCE).bbl
	TEXINPUTS="./styles:" latex $<
	TEXINPUTS="./styles:" latex $<
	
ps: $(SOURCE).ps
$(SOURCE).ps: $(SOURCE).dvi
	dvips -Ppdf -G0 -ta4 -o $@ $<

pdf: $(SOURCE).pdf
$(SOURCE).pdf: $(SOURCE).ps
	ps2pdf -dSubsetFonts=true -dMaxSubsetPct=100 -dEmbedAllFonts=true -dUseFlateCompression=true -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress $< $@

bib: $(SOURCE).bbl
$(SOURCE).bbl : $(SOURCE).bib
	TEXINPUTS="./styles:" latex $(SOURCE).tex
	TEXINPUTS="./styles:" bibtex $(SOURCE)1
	TEXINPUTS="./styles:" bibtex $(SOURCE)2
	rm -f $(SOURCE).dvi

report: $(SOURCE).pdf
	pdffonts $< > fonts.log
	cat fonts.log


show: pdf
	kill -9 `ps aux | grep $(SOURCE).pdf | grep -grepv grep | awk '{print $2}'` >/dev/null 2>&1 || true
	gnome-open $(SOURCE).pdf

#clean:
#	rm -rf *.bbl *.blg *.out *.aux *.dvi *.log *.ps *.backup *~ *.bak *.toc *.idx *.DS_Store ./*/*.aux

clean:
	for e in $(OBJS); do find . -path './.git' -prune -o -name "*.$$e" -exec rm -f \{\} \; ; done
	find . -path './.git' -prune -o -name "*~" -exec echo rm -f \{\} \;

deep-clean: clean
	rm -rf *.bbl *.pdf *.fdb_latexmk


