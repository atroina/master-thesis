\section{RTLib JNI bridge}
\label{sec:rtlib}
As described in Section \ref{sec:bosp}, the \emph{Run-Time Library (RTLib)}
supports the application which needs to interact with Barbeque Framework, by
providing a rich set of features as well as supporting the application-specific
run-time management activities. When some \emph{JNI} mentions were made in
Section \ref{sec:jni}, and deeply described in Appendix \ref{app:Tutorials},
whenever it is needed to expose some native functions to the Java side, it is
required to express their signatures following a very specific pattern, let's
analyse a brief example: if we need to call a native function \texttt{foo} from
within a Java class \texttt{ExampleClass}, which belongs to the package
\texttt{com.examples.my}, there are two main steps we have to take:

\begin{enumerate}
	\item Declare within the Java class the method\\
			\texttt{public native void foo();}\\
			without any implementation, so that it's visible to the class
	\item Implement a native function with the specific signature:\\
			\texttt{Java\_com\_example\_my\_foo(JNIEnv *env, jobject thiz)}
\end{enumerate}

This being said, to expose \emph{RTLib} native functions, we had to create a
native wrapper class, which makes all the already working functions accessible
to Java through the appropriate signature, wrapping each call within its
correspondent JNI name. An example of a native signature from Barbeque's RTLib
can be seen at Listing \ref{lst:rtlib1}, its wrapping function to expose it
through JNI can be seen at Listing \ref{lst:rtlib2}, and the Java declaration
and usage is shown at \ref{lst:rtlib3}.

\begin{lstlisting}[language=C,
		columns=fullflexible,
		showstringspaces=false,
		xleftmargin=15pt,
		frame = l,
		numbers=left,
		commentstyle=\color{gray}\upshape,
		caption=RTLib native function signature from rtlib.h,
		label=lst:rtlib1]
RTLIB_ExitCode_t RTLIB_Init(const char* name, RTLIB_Services_t **services);
\end{lstlisting}

\begin{lstlisting}[language=C,
		columns=fullflexible,
		showstringspaces=false,
		xleftmargin=15pt,
		frame = l,
		numbers=left,
		commentstyle=\color{gray}\upshape,
		caption=RTLib wrapping example of Listing \ref{lst:rtlib1},
		label=lst:rtlib2]
//Global reference to the RTLib instance
RTLIB_Services_t *rtlib = NULL;

// RTLIB_ExitCode_t RTLIB_Init(const char *name, RTLIB_Services_t **rtlib)
JNIEXPORT jint
Java_it_polimi_dei_bosp_BbqueService_RTLIBInit(
		JNIEnv *_env, jobject _thiz,
		jstring _name) {
	const char *name = _env->GetStringUTFChars(_name, 0);
	RTLIB_ExitCode_t result;

	obj = (jobject)_env->NewGlobalRef(_thiz);

	result = RTLIB_Init(name, &rtlib);
	if (result != RTLIB_OK) {
		LOGE("RTLIB initialization failed");
		return (-result);
	}

	LOGI("RTLIB initialization done");
	return RTLIB_OK;
}
\end{lstlisting}

\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=Java declaration and usage of the JNI function declared at
				   Listing \ref{lst:rtlib2} within the Java class
				   \texttt{it.polimi.dei.bosp.BbqueService},
		   label=lst:rtlib3]
public native int RTLIBInit(String mode);

public void onCreate() {
	Log.d(TAG, "onCreated");
	creationTime = System.currentTimeMillis();
	super.onCreate();
	int response;
	response = RTLIBInit("test");
	Log.d(TAG, "Response from RTLIBInit is:"+response);
}
\end{lstlisting}

The aforementioned wrapping concerns the Java-to-Native call. Barbeque Framework
strongly relies on callbacks, which were wrapped as well as straight calls.\\
In Listing \ref{lst:rtlib4}, an example of the callback wrapping for the
\texttt{onRun} function is shown: the framework will call the \emph{onRun}
function as usual, but the Java callback will be performed, thanks to the\\
\texttt{env->CallIntMethod(obj, cb[ON\_RUN].method)}\\
The way we chose to proceed is mutuated from the \emph{Tutorial3} shown into
Appendix \ref{sec:tutorial3}. During the initialization of the library, a call
to fill a \emph{callbacks array} is called, where the structure
\texttt{callback\_t}  of the array is shown in Listing \ref{lst:rtlib5}

\begin{lstlisting}[language=C,
		columns=fullflexible,
		showstringspaces=false,
		xleftmargin=15pt,
		frame = l,
		numbers=left,
		commentstyle=\color{gray}\upshape,
		caption=RTLib Bridge - wrapping example for the onRun callback,
		label=lst:rtlib4]
RTLIB_ExitCode_t
BbqueAndroid::onRun() {
	LOGD("Callback onRun(), %d", Cycles());
	if (env->CallIntMethod(obj, cb[ON_RUN].method))
		return RTLIB_EXC_WORKLOAD_NONE;
	return RTLIB_OK;
}
\end{lstlisting}

\begin{lstlisting}[language=C,
		columns=fullflexible,
		showstringspaces=false,
		xleftmargin=15pt,
		frame = l,
		numbers=left,
		commentstyle=\color{gray}\upshape,
		caption=RTLib Bridge - callbacks structure,
		label=lst:rtlib5]
typedef struct {
	const char* name;
	const char* signature;
	jmethodID method;
} callback_t;

typedef enum {
	ON_SETUP = 0,
	ON_CONFIGURE,
	ON_SUSPEND,
	ON_RESUME,
	ON_RUN,
	ON_MONITOR,
	ON_RELEASE,
	CB_COUNT // This must be the last entry
} cbid_t;

static callback_t cb[CB_COUNT];
\end{lstlisting}
The makefile \texttt{Android.mk} will let this library being built as a
\emph{shared library}: how it is done, is shown in Appendix
\ref{app:Tutorials}.\\
To sum up, this first part of the work consisted in creating some native code to
wrap the pre-existing functions into \emph{JNI-compliant} signatures, and the
calls to native callbacks into proper \emph{JNI-based} Java method
invocations.

This being done, all that remains will be coded within the Java environment:
it's easy to understand how writing this small portion of native code helped us
in adapting the \emph{RTLib} instead of writing all from scratch in Java
language.\\
Now, we can analyse how the integration into Android has been implemented, and a
full-working Android Java API to Barbeque has been developed.
