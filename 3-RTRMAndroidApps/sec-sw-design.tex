\section{Software design}
\label{sec:sw-design}

To make \emph{RTLib} functions available to the Android Apps developer an
Android Service has been developed. The basic idea is to provide the developer
with a main \texttt{Service} class that can be extended by a customized service,
that will actually implement the callback methods, so that they will be executed
whenever Barbeque will call them.

\subsection{BbqueService}
\label{subs:bbqueservice}

A Service is an application component that can perform long-running operations
in the background and does not provide a user interface. Another application
component can start a service and it will continue to run in the background even
if the user switches to another application, which is exactly what we needed:
our Service "maps" the RTLib, and it's a kind of interface to that, always
running and handling the interaction with Barbeque. Additionally, a component
can bind to a service to interact with it and even perform interprocess
communication (IPC): we are going to analyse this communication aspect in
\ref{ssub:messaging}.\\
A service is \emph{"bound"} when an application component (like an
\emph{Activity}) binds to it by calling \texttt{bindService()}. A bound service
runs only as long as another application component is bound to it.  Multiple
components can bind to the service at once, but when all of them unbind, the
service is destroyed. To properly bind a service, you need to implement a couple
of callback methods: \texttt{onStartCommand()} to allow components to start it
and \texttt{onBind()} to allow binding.  Our Service is called
\texttt{BbqueService}, and obviously extends the Android \texttt{Service} class,
and is going to be a \emph{bound} service, since we will need to open a
communication channel between it and the Activity.\\
A schema of the bound service life cycle is depicted at Figure
\ref{fig:service-lc}.\\
The first operation that the service class will perform when instantiated will
be loading the shared library, as shown in Listing \ref{lst:service1}.

\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=Service loads shared library,
		   label=lst:service1]
static {
	System.loadLibrary("bbque_rtlib");
}
\end{lstlisting}


\begin{figure}[t]
	\centering
	\includegraphics[width=\mediumfigure]{images/service-bound.eps}
	\caption[Bound service life cycle]
			{\figurecaption{Bound service life cycle}}
	\label{fig:service-lc}
\end{figure}

The \texttt{BbqueService} mainly consists of three sections:
\begin{itemize}
	\item Native methods declaration
	\item Callbacks implementation
	\item Messaging handling
\end{itemize}

\subsubsection{Native methods declaration}
\label{ssub:native}

The \texttt{BbqueService} declares all the signatures corresponding to the
native functions he wants to access as "\texttt{public native}", as we saw for
example at line 1 of Listing \ref{lst:rtlib3}. Those methods are called whenever
needed, within the service class, through proper public methods, that are
visible to the \emph{custom service} that will extend it: doing so, the
developer is able to create her own Android service which extends our
\texttt{BarbequeService} and  accesses to RTLib native methods.

The main RTLib functions were exposed as native calls, which can be performed
within the \emph{service} (and therefore any activity which is bound to it), by
sending the proper message: in Table \ref{tab:native-f} the message code and the
native method correspondence is shown.

\begin{table}[h]
		\centering
		\begin{tabular}{r|l}
				\hline
				\textbf{Message code}&\textbf{Native method}\\
				\hline
				MSG\_ISREGISTERED&EXCisRegistered()\\
				MSG\_CREATE&EXCCreate(name,recipe)\\
				MSG\_START&EXCStart()\\
				MSG\_WAIT\_COMPLETION&EXCWaitCompletion()\\
				MSG\_TERMINATE&EXCTerminate()\\
				MSG\_ENABLE&EXCEnable()\\
				MSG\_DISABLE&EXCDisable()\\
				MSG\_GET\_CH\_UID&EXCGetChUid()\\
				MSG\_GET\_UID&EXCGetUid()\\
				MSG\_SET\_CPS&EXCSetCPS()\\
				MSG\_SET\_CTIME\_US&EXCSetCTimeUs()\\
				MSG\_CYCLES&EXCCycles()\\
				MSG\_DONE&EXCDone()\\
				MSG\_CURRENT\_AWM&EXCCurrentAWM()\\
				\hline
		\end{tabular}
		\caption{The correspondence between message code (left) and native
				method called (right) is shown. Each message code can be used to
				execute calls from the Activity to the Service, and vice versa:
				this means that for each task to accomplish, the activity and
				the service have a unique code which identifies the task, and
				sets up the communication}
		\label{tab:native-f}
\end{table}

By extending the Android \texttt{Service} class, our \texttt{BbqueService}
overrides the \texttt{onCreate()} method, which is called whenever a service is
instantiated and, in our specific case, also \emph{bound}.

\subsubsection{Callbacks implementation}
\label{ssub:callbacks}

Basically the \emph{service} declares, in addition to native methods, all the
callback methods needed by the native \emph{RTLib} wrapper. They just send a
broadcast intent, to notify they've been called. They can then be overridden by
the \emph{customized service} that extends the \texttt{BbqueService}, so that
the callback from the native library will be then diverted to the service-to-be.
In Listing \ref{lst:rtlib4} the native JNI callback for the \texttt{onRun}
method is shown: its Java counterpart can be found at Listing
\ref{lst:callback}.

\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=Example for the onRun callback - Service side,
		   label=lst:callback]
public int onRun() {
	Log.d(TAG,"onRun called");
	intent.putExtra("BBQ_DEBUG", "onRun called");
	intent.putExtra("INTENT_TIMESTAMP",
			System.currentTimeMillis()-creationTime);
	intent.putExtra("APP_NAME", name);
	sendBroadcast(intent);
	return 0;
}
\end{lstlisting}

In addition to the \texttt{onCreate}, the callbacks the developer can customise
by overriding can be found in Table \ref{tab:callbacks};

\begin{table}[h]
		\centering
		\begin{tabular}{l}
				\hline
				\textbf{Callback}\\
				\hline
				onSetup()\\
				onConfigure(int)\\
				onSuspend()\\
				onResume()\\
				onRun()\\
				onMonitor()\\
				onRelease()\\
				\hline
		\end{tabular}
		\caption{Apart from the onRun() method overriding, which is mandatory to
				let the application execute its processing block when Barbeque
				calls back, the methods above listed can be (and should be)
				implemented as well, to properly exploit the run-time
				auto-configuration of the application.}
		\label{tab:callbacks}
\end{table}
\subsubsection{Message handling}
\label{ssub:messaging}

A bound service offers a client-server interface that allows components to
interact with the service, send requests, get results, and even do so across
processes with interprocess communication (IPC). Consequently to our approach of
implementing a service to act as an interface to \emph{RTLib}, we needed a
lightweight and well structured messaging protocol to allow the \emph{activity}
communicating with the \emph{service}, and by this, to the \emph{native
library}. This protocol was identified into the \texttt{Messenger} class. Using
\texttt{Messenger} is the simplest way to perform interprocess communication
(IPC), because it queues all requests into a single thread so that you don't
have to design your service to be thread-safe. The service defines a Handler
(Listing \ref{lst:messenger1}, line 1) that responds to different types of
\emph{message} objects. This Handler is the basis for a \texttt{Messenger} that
can then share an \textbf{IBinder} with the client (Listing \ref{lst:messenger1}
line 20-21), allowing the client to send commands to the service using
\emph{message objects} (Table \ref{tab:native-f}). Additionally, the client can
define a \texttt{Messenger} of its own so the service can send messages back: a
double definition of a proper Handler within the two classes (client and server)
lets the two communicate bidirectionally. As an implementing choice, we decided
that the two directions of the same task (e.g. starting the processing from the
activity, and receiving the response by the service) share the same message
code.

\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=Messenger protocol implementation - Service side,
		   label=lst:messenger1]
protected class BbqueMessageHandler extends Handler {
	@Override
	public void handleMessage(Message msg) {
		switch (msg.what) {
			case MSG_ISREGISTERED:
				isRegistered(msg.replyTo);
				break;
			case MSG_CREATE:
				create(msg.replyTo, msg.obj);
				break;
			default:
				super.handleMessage(msg);
		}
	}
}

final Messenger mMessenger = new Messenger(new BbqueMessageHandler());
		   
@Override
public IBinder onBind(Intent intent) {
	return mMessenger.getBinder();
}
\end{lstlisting}

What is shown in Listing \ref{lst:messenger1} is a portion of the service code
needed to properly handle the receiving of messages sent by another component,
typically the caller activity. As can be guessed, the parameter \texttt{replyTo}
at lines 6 and 9, contains the reference to the dual \texttt{Messenger} object
belonging to the caller: hence through this reference the service is able to
reply with its own response message to the caller, as shown in Listing
\ref{lst:messenger2}, line 12.

\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=Messenger protocol implementation - Service side,
		   label=lst:messenger2]
protected void create(Messenger dest, Object obj) {
	String messageString = obj.toString();
	String params[] = messageString.split("#");
	name = params[0];
	recipe = params[1];
	Log.d(TAG, "create, app: "+name+" with recipe "+recipe);
	int response = EXCCreate(name, recipe);
	Message msg = Message.obtain(null, MSG_CREATE,
						response,
						0);
	try {
		dest.send(msg);
		replyTo = dest;
	} catch (RemoteException e) {
		e.printStackTrace();
	}
}
\end{lstlisting}

The \texttt{Message.obtain(...)} can be used with different signatures, and
Android designers provided the developers with some standard parameters, to be
used in basic cases, and typically there are two \texttt{int} called
\texttt{arg1} and \texttt{arg2} as well as a generic \texttt{Object} called
\texttt{obj} which, as a only constraint, has to implement the
\texttt{Parcelable} interface. The overhead added by a message, in terms of
time, is around 170\textasciitilde200 microseconds per message.

Being clear how the \texttt{BbqueService} was implemented, and how the messaging
protocol works, we can briefly see how a developer can set up her own Activity.

\subsection{Activity: common aspects}
\label{subs:act-common}
In Section \ref{sec:apps} two sample activities that were realised within
this work are deeply analysed and commented, but here some common aspects, and
best practices to follow when developing applications that need to interact with
Barbeque framework are described.\\
The \emph{bound service} concept was broadly described above: the
\emph{activity} has to \textbf{bind} itself to the service, and this is
typically done withing the \texttt{onStart()} callback method, which is
overridden from the parent main \texttt{Activity} class, as shown in Listing
\ref{lst:activity-bind}, where the \texttt{CustomService.class} which appears at
line 5 is a service which extends our \texttt{BbqueService}.

\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=Activity: bind to a Service,
		   label=lst:activity-bind]
@Override
protected void onStart() {
	super.onStart();
	// Binding to the service. 
    bindService(new Intent(this, CustomService.class),
			mConnection, Context.BIND_AUTO_CREATE);
}
\end{lstlisting}

The \texttt{mconnection} object that we pass to the \texttt{bindService} method
is an object of type \texttt{ServiceConnection}, and its construction is shown in
Listing \ref{lst:activity-connection}. The \texttt{mService} object which
appears at lines 5 and 10 is the one we use to send messages to the
\texttt{CustomService}: obviously, given that the latter extends the
\texttt{BbqueService}, each message sent through the \texttt{mService} will be
sent to this one as well.

\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=Activity: creation of the \texttt{ServiceConnection} object,
		   label=lst:activity-connection]
private final ServiceConnection mConnection = new ServiceConnection() {
	@Override
	public void onServiceConnected(ComponentName className,
			IBinder service) {
		mService = new Messenger(service);
		mBound = true;
	}
	@Override
	public void onServiceDisconnected(ComponentName className) {
		mService = null;
		mBound = false;
	}
};
\end{lstlisting}

The \emph{message handling} into the activity is absolutely dual with respect to
the one described at \ref{ssub:messaging}.
