\section{Testing applications}
\label{sec:apps}

With the \emph{JNI wrapper}, properly compiled, and the Java class which
implements the \emph{Barbeque service}, we are ready to test the Barbeque
framework by analysing two Java applications specifically developed for this
purpose. The first one just tests the calls to Barbeque, and the callbacks from
Barbeque, while the second one consists of a \emph{face detection} application
which uses a library by \emph{ST Microelectronics} developed to work on the
many-core \emph{STHORM} platform, codename \emph{P2012}. Such a platform will be
the ideal application field for a RTRM as Barbeque.

\subsection{BOSP testing application}
\label{sec:bosp-testapp}

With respect to what has been described in the previous sections, to build a
customised application the developer has to create a \emph{service} - which
extends the \texttt{BbqueService} and one (or more than one) \emph{activity}.\\

\paragraph{Custom service}

This very compact \texttt{CustomService} basically does three main things:

\begin{itemize}
	\item Declares a \texttt{CustomeMessageHandler}, which extends the default
			barbeque message handler \texttt{BbqueMessageHandler} (Listing
			\ref{lst:messenger1}, line 1) to handle new custom messages others
			than the default ones (in which cases it will call the
			\texttt{super} implementation of the handler. An example can be seen
			at Listing \ref{lst:bosp-testapp1}
\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=\texttt{CustomService}: message handler overriding,
		   label=lst:bosp-testapp1]
class CustomMessageHandler extends BbqueMessageHandler {
	@Override
	public void handleMessage(Message msg) {
		switch (msg.what) {
		case MSG_CYCLES:
			Log.d(TAG, "Message cycles setting: " + msg.arg1);
			cycle_n = msg.arg1;
			break;
			default:
			super.handleMessage(msg);
		}
	}
}
\end{lstlisting}
			In our implementation, the custom message added to the default ones
			will be used to set, from the activity, the number of \emph{onRun}
			cycles to be performed.
	\item Overrides the \texttt{onBind} method, to return an \texttt{IBinder}
			interface to the \emph{activity} that will execute the binding,
			which now links to its own instance of \texttt{Messenger} object, as
			shown in Listing \ref{lst:bosp-testapp2}
\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=\texttt{CustomService}: onBind overriding,
		   label=lst:bosp-testapp2]
final Messenger cMessenger = new Messenger(new CustomMessageHandler());

@Override
public IBinder onBind(Intent intent) {
	return cMessenger.getBinder();
}
\end{lstlisting}
	\item Overrides Barbeque callbacks methods or, at least, the \texttt{onRun},
			which is mandatory to have a working application. The code related
			to this overriding is shown at Listing \ref{lst:bosp-testapp3}, and
			a screenshot of the execusion can be found at Figure
			\ref{fig:bosp-testapp}, where the displayed information in the
			\texttt{TextView} \circled{1} comes from lines 5-6 of the
			aforementioned listing.
\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=\texttt{CustomService}: onRun overriding,
		   label=lst:bosp-testapp3]
@Override
public int onRun() {
	int cycles = EXCCycles();
	Log.d(TAG, "onRun called, cycle: " + cycles);
	intent.putExtra("BBQ_DEBUG", "onRun called, cycle: " + cycles);
	sendBroadcast(intent);
	try {
		Thread.sleep(1000);
		if (cycles >= cycle_n)
			return 1;
	} catch (InterruptedException e) {
		}
	return 0;
}
\end{lstlisting}
\end{itemize}

This being done, the \texttt{CustomService} is ready to be bound to an
application component, typically our activity, and to interact with Barbeque
native code as well. The binding configuration must be explicitly declared in
the configuration file named \texttt{AndroidManifest.xml}, which is local to any
Android application. Within the \texttt{<application>} node, the following line
must be added:\\
\texttt{<service android:name=".CustomService" ></service>}

\paragraph{Testing activity}

The activity developed to test the integration of the Barbeque basic features is
shown in Figure \ref{fig:bosp-testapp}. Below each part, with its related
behind-code is analysed.

\begin{figure}[h]
	\centering
	\includegraphics[width=\mediumfigure]{images/bbqueapp.eps}
	\caption[Barbeque testing application screenshot]
			{\figurecaption{Barbeque testing application screenshot}}
	\label{fig:bosp-testapp}
\end{figure}

\begin{enumerate}[label=\protect\circled{\arabic*}]
	\item \texttt{TextView} shows messages which come as Barbeque
			\emph{responses} to methods invocations through the messaging
			protocol
	\item \emph{isRegistered} button triggers the dispatch of the
			\texttt{MSG\_IS\_REGISTERED} message, which is directly handled by
			the \texttt{BbqueService} as shown at Listing \ref{lst:messenger1}
			line 5
	\item \emph{Create} button triggers the dispatch of the \texttt{MSG\_CREATE}
			message, which is directly handled by the \texttt{BbqueService}.
			For the sake of clarity, the code behind this button is shown at
			Listing \ref{lst:bosp-testapp4}, to be considered ad part of the
			flow received by the \emph{service} as shown at Listing
			\ref{lst:messenger1} line 8 and, from there, forwarded to Listing
			\ref{lst:messenger2}.
\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=\texttt{BbqueActivity}: Create button code,
		   label=lst:bosp-testapp4]
public void btnCreate(View v) {
	Log.d(TAG, "Create button pressed...");
	if (!mBound) return;
	Message msg = Message.obtain(null, CustomService.MSG_CREATE, 0, 0);
	msg.obj = APP_NAME+"#"+APP_RECIPE;
	try {
		msg.replyTo = mMessenger;
		mService.send(msg);
	} catch (RemoteException e) {
		e.printStackTrace();
	}
}
\end{lstlisting}
	\item \emph{Terminate} button triggers the dispatch of the message
			\texttt{MSG\_TERMINATE}, which at the end, in the \emph{service}
			calls the \texttt{EXCTerminate()}
	\item \texttt{EditText} field is used to set the number of \emph{onRun}
			cycles we want the application to execute. When the current cycles
			number exceeds the upper bound, the execution is terminated, and
			Barbeque calls the \emph{onRelease} callback.
	\item \emph{Start} button triggers the dispatch of the \texttt{MSG\_CYCLES}
			which is handled by the \texttt{CustomService} to set a local
			variable, and consequently the  \texttt{MSG\_START} message, is
			sent, and handled by the \texttt{BbqueService}
	\item \emph{Broadcast Console} button opens the \emph{Bbque Broadcast
			Console} \circled{8} which basically outputs all the intents
			broadcasted by the \emph{service}. More on this will be discussed
			further.
\end{enumerate}

To receive \emph{intents} sent by the \emph{service}, and output them into the
\texttt{TextView} \circled{1}, a listener to them has been enabled. At first a
\texttt{BroadcastReceiver} is created, which overrides the \texttt{onReceive}
methods in which body the code to be executed whenever an intent it's receiver
of is contained. The code is shown at Listing \ref{lst:bosp-testapp5}
How these \emph{intents} are broadcast by the \emph{service} is described within
the next paragraph.
\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=\texttt{BbqueActivity}: Intent handling,
		   label=lst:bosp-testapp5]
IntentFilter receiverFilter = new IntentFilter ();
BroadcastReceiver receiver = new BroadcastReceiver() {
	@Override
	public void onReceive(Context context, Intent intent) {
		String bbqDebugIntent = intent.getStringExtra("BBQ_DEBUG");
		output.setText(bbqDebugIntent);
	}
};
@Override
public void onCreate(Bundle savedInstanceState) {
	//...
	receiverFilter.addAction("it.polimi.dei.bosp.BBQUE_INTENT");
	registerReceiver(receiver, receiverFilter);
	//...
}
\end{lstlisting}

\paragraph{Barbeque Broadcast Console}

\emph{Barbeque Broadcast Console (BBC)} is an activity, called by the main
Barbeque activity we've seen in the previous paragraph, which explores the
possibilities offered by the \emph{intents mechanism} provided by Android.\\
\emph{Intents} are asynchronous messages which allow Android components to
request functionalities from other components of the Android system. For example
an Activity can send an Intent to the Android system which starts another
Activity. Therefore Intents allow to combine loosely coupled components to
perform certain tasks. An Intent can also contain data.\\ The \emph{BBC}
implements a \texttt{BroadcastReceiver} as seen for the previous activity
(Listing \ref{lst:bosp-testapp5}), but is able to retrieve much more
information, from the intent it has registered a filter for with the method:\\
\texttt{receiverFilter.addAction("it.polimi.dei.bosp.BBQUE\_INTENT");}\\
as show in Listing \ref{lst:bosp-testapp6}
\begin{lstlisting}[language=Java,
		   columns=fullflexible,
		   showstringspaces=false,
		   xleftmargin=15pt,
		   frame = l,
		   numbers=left,
		   commentstyle=\color{gray}\upshape,
		   caption=\texttt{BBCActivity}: Intent handling,
		   label=lst:bosp-testapp6]
BroadcastReceiver receiver = new BroadcastReceiver() {
	@Override
	public void onReceive(Context context, Intent intent) {
		String entry = String.format("[%7.3f - %-15s] %s \n",
			((float)(intent.getLongExtra("INTENT_TIMESTAMP",0)))/1000,
			intent.getStringExtra("APP_NAME"),
			intent.getStringExtra("BBQ_DEBUG"));
		console.append(entry);
		final int scrollAmount = console.getLayout().getLineTop(
				console.getLineCount())-console.getHeight()+10;
	    // if there is no need to scroll, scrollAmount will be <=0
	    if(scrollAmount>0)
	        console.scrollTo(0, scrollAmount);
	    else
	        console.scrollTo(0,0);
	}
};
\end{lstlisting}
Whenever an intent from the \emph{service} is received, the \emph{action}
retrieves all the attributes associated to that very specific intent.\\
In Listing \ref{lst:callback} the intent dispatching from within the
\emph{service} is shown, with regard to the \emph{onRun} callback. A partial log
of the output is shown at \circled{8} in Figure \ref{fig:bosp-testapp}.

\subsection{STHORM Face Detection}
\label{sec:sthorm-fd}

\emph{STHORM} (codename Platform2012) is a many-core embedded architecture that
has been designed by STMicroelectronics and by CEA as a scalable and
customizable acceleration device. \cite{Melpignano2012} A test chip in 28nm is
being sampled now, featuring 69 processors in less than 20mm2. Our testing
application currently runs on an emulated platform, but is meant to show one of
the many possible applications that can be coded directly in Java, and supported
by the BarbequeRTRM, which is being deployed to be tested on the aforementioned
platform.\\ This application has been largely reimplemented, and integrated to
exploit our new \texttt{BbqueService}, which is the core of this work.\\
A sequence diagram to clarify the whole process is provided at Figure
\ref{fig:sthorm-sequence}

\begin{figure}[h]
	\centering
	\includegraphics[width=\widefigure]{images/STHorm_sequence.eps}
	\caption[STHORM Face Detection activity - Sequence diagram]
			{\figurecaption{STHORM Face Detection activity - Sequence diagram}}
	\label{fig:sthorm-sequence}
\end{figure}

We mainly have five actors, and the communication between the \emph{activity}
and the \emph{service} is implemented exploiting the messaging protocol we
mentioned before.\\
In Figure \ref{fig:sthorm-sequence}, four main steps have been identified.

\begin{enumerate}[label=\protect\circled{\arabic*}]
	\item The \emph{activity} initialises the \texttt{Camera} object. The
			preview feature of the \emph{Android Camera} has been chosen: it
			already outputs camera frames to a \texttt{Surface} object, and
			its frame rate is high enough to give to the user a good user
			experience. On top of this \emph{surface} we put an
			\texttt{ImageView} object, where we print, if and whenever
			available, the rectangles indicating the faces within the
			picture.\\
			Whenever a new frame is available from the camera, it is shown on
			the surface, and a callback is executed by default within Android:
			within the method \texttt{onPreviewFrame}, it's possible to save
			(for example into a double buffer, as we did) frames for future use.
	\item The \emph{Service} mentioned is an abstraction of both the basic
			\texttt{BbqueService} and a customized \texttt{STHormFDService},
			which extends the former, and implements its own processing code,
			including the \texttt{onRun} overriding. Thus, whenever an
			\emph{onRun} callback is performed by \emph{Barbeque}, a
			\emph{message} is sent to the \emph{activity}, asking for a new
			frame. The activity replies sending back the most recent frame,
			taken from the aforementioned double buffer.
	\item The \emph{Service} has received the frame, and calls some native code
			to process it: this is possible, again, through a JNI call. The
			\emph{Service} gets back an array with the number of faces found
			into the frame and, if any, the coordinates of the rectangles
			corners that enclose each face.
	\item The \emph{Service} dispatches the retrieved information to the
			\emph{activity}, through a \emph{message}, and rectangles are drawn
			upon the camera preview surface.
\end{enumerate}

The previous enumeration is repeated for any frame the \emph{onRun} is able to
process, at the speed set by the user, from the slider element
(\texttt{SeekBar}) available on the interface.
