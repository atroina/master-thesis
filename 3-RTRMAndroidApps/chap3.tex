\chapter{Run-Time Resource Management of Android Applications}
\label{cha:RTRM-AA}
\chapCite{My favorite things in life don't cost any money.\\
		It's really clear that the most precious resource\\
		we all have is time.}
{Steve Jobs}

In the previous chapter, was described what the \emph{BOSP} project is, and how
it is structured, and some elements about JNI were given as well.\\
As previously mentioned, Barbeque is developed in native (C/C++) code and, as a
consequence of its characteristic of portability, an Android-ready build can be
deployed directly from its menu (through the \texttt{make menuconfig} command).
Once Barbeque has been built, the aforementioned package is automatically
deployed to the first device reachable by the \emph{Android Debug Bridge (adb)},
a versatile command line tool that lets you communicate with an emulator
instance or connected Android-powered device. Once Barbeque is deployed to an
Android system, it is possible to run it, and to write native applications that
exploit its functionalities, and implement its callback methods.\\
But Android applications, as it is known, are developed in Java language:
\emph{so, how can we let Android developers building their own Java applications
which interact with Barbeque?}\\
The answer will be described in this chapter, and sometimes documented with
short code listings and clarifying figures.\\
To have a general idea of how the whole stack is structured, and where this work
gives its contribute, we can analyse Figure \ref{fig:block-diagram}. 

\begin{figure}
	\centering
	\includegraphics[width=\widefigure]{images/block-diagram-stream.eps}
	\caption[System block diagram]
			{\figurecaption{System block diagram}}
	\label{fig:block-diagram}
\end{figure}

Three main sections can be identified within the aforementioned diagram: the
Java, the native, and the hardware one. The system shown is clearly an
heterogeneous one, as we can see in the hardware section, where there's a
\emph{GPGPU} platform (typically it will be identified, within this work, as a
specific many-core accelerator platform by STMicroelectronics), and a general
\emph{Host} platform, which can be a multi-core processor.\\
In the first one, which is \emph{Java-coded} and \emph{Android-specific}, an
\emph{activity} and a \emph{service} can be found: the former corresponds to a
user screen, and it's the class which directly interacts with the user, the
latter corresponds to a generic android service which extends our
\texttt{BarbequeService}. This service class statically loads, when created, the
shared object which maps the native library (Listing \ref{lst:service1}). The
activity and the service communicate to each other thanks to a messaging
paradigm, already implemented in \emph{Android} and largely discussed in
\ref{subs:bbqueservice}. Moving from here to the second section, the native one,
the first block we encounter is what we called \emph{JNI Bridge}, a portion of
code that represents a sort of midland between the pure Java code, and the pure
native code: this intermediate code is, precisely, the \emph{Java-Native
Interface (JNI)}. This block, along with the block which lays below it, is built
as \emph{shared library}, as indicated with the dashed line. The native
libraries can be found right below the JNI bridges: in the figure we put the
\emph{RTLib} (Sections \ref{sec:bosp}, \ref{sec:rtlib}), and a generic
\emph{GPGPU} library. The latter is inserted into a \emph{GPGPU SDK}, while the
former is part of the \emph{BarbequeRTRM}, and lets any application interacting
to it, through the already described native command and callbacks (Section
\ref{sec:bosp}). Both the communication streams between the \emph{GPGPU SDK} and
the \emph{BarbequeRTRM} go through the \emph{Linux Kernel} and the respective
\emph{drivers} to the last section, the \emph{hardware} one. Here the hardware
platforms are placed: the \emph{GPGPU} with its own (reconfigurable) firmware,
the Host cpu, the memory the two share, and the possible peripherals (eg. a
webcam, or a keyboard). \emph{BarbequeRTRM} interacts with both the \emph{GPGPU}
and the \emph{Host} processors, controlling the resources availability and
applying the best possible both hardware and software configuration to provide
the best \emph{run-time} experience and resources allocation.

\paragraph{Stream example:}
From within the \emph{Activity}, two messages are dispatched, and delivered to
the \texttt{BbqueService} class:

\begin{itemize}
		\item \texttt{MSG\_CREATE}: to create an execution context. This command
				is executed on the \emph{shared library} which calls the native
				function in charge of creating an \emph{RTLib} instance and
				register it to the Barbeque manager. If everything goes
				smoothly, a specific value is returned.
		\item \texttt{MSG\_START}: to kick start the Barbeque \emph{state
				machine} (see Figure \ref{fig:aem}): from this very moment on,
				Barbeque typically executes its cycle of \texttt{onRun} and
				\texttt{onMonitor} callbacks along with potential others,
				depending on the resource availability and needs (e.g. it may
				call the \texttt{onConfigure}, when the application
				reconfiguration is requested)
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=\mediumfigure]{images/block-diagram-stream-messages.eps}
	\caption[Call flow MSG\_START]
			{\figurecaption{Call flow for MSG\_START}}
	\label{fig:msg-start-flow}
\end{figure}

What happens during the aforementioned calls is shown in Figure
\ref{fig:msg-start-flow}. The activity (in this specific case the call is
triggered by a button, from the user interface) obtains a \emph{message} and
sends it to the correspondent instance of the \emph{service} \circled{1}: the
latter executes the native call \circled{2} on the shared library, which embeds
an instance of the RTLib that, in turn, interacts with Barbeque \circled{3}.
Barbeque performs its own checks and routines \circled{4} and its response -
which is returned as a native value by the RTLib to the JNI bridge \circled{5} -
is then sent back by the \emph{service} to the \emph{activity} through the
\emph{message} with the correspondent label \circled{6}.

This being done, while \emph{Barbeque} keeps calling the \texttt{onRun}, which
executes the elementary piece of code needed to process an atomic element of the
stream (e.g. a frame of a video stream), the need for the application
reconfiguration could arise: if so, \emph{Barbeque} receives a signal from the
hardware layer, and notifies this situation to the \emph{Service}, by executing
the \texttt{onConfigure} callback; depending on the implementation of this
callback method, the application reconfigures its settings to perform at best
according to the available resources.\\
The \texttt{onRun} cycle is illustrated at Figure \ref{fig:onrun-flow}:
depending on the system status, and on the constraints currently active,
\emph{BarbequeRTRM} calls the \texttt{onRun} function on the \texttt{RTLib}
instance corresponding to the application that it wants to allow running
\circledchar{a}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\mediumfigure]{images/block-diagram-stream-onRun.eps}
	\caption[onRun-cycle diagram]
	{\figurecaption{onRun-cycle diagram}}
	\label{fig:onrun-flow}
\end{figure}

This class is extended by the JNI bridge that we implemented,
\circled{b} which contains the implementation of the JNI call to be performed
when deployed under Android. Typically it consists of a callback method on the
Java class corresponding to the Barbeque service \texttt{BbqueService}
\circledchar{c}. The \texttt{onRun} method is overridden by the
\texttt{CustomService} which contains the actual implementation, the piece of
code to be repeatedly executed anytime a \emph{Barbeque} \texttt{onRun} call
occurs \circled{d}: this execution could be performed locally to the service or,
most typically, involving external information coming from the activity, or some
computations to be performed by a specific hardware. In the former case, the
service is able to require data (e.g. camera frames, audio samples) to the
activity, and send back processed data to the activity, through the already
mentioned messaging mechanism \circledchar{e}. In the latter case, the data
which the service needs to process, can be passed to the native code \circled{f}
which executes right on the GPGPU hardware. Each \texttt{onRun} can basically
involve the aforementioned operations.

%RTLib
\input{3-RTRMAndroidApps/sec-rtlib}
%Software design
\input{3-RTRMAndroidApps/sec-sw-design}
%Testing applications
\input{3-RTRMAndroidApps/sec-apps}
